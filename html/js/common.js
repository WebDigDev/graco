window.onload = function() {

    //Chrome Smooth Scroll
    try {
        $.browserSelector();
        if ($("html").hasClass("chrome")) {
            $.smoothScroll();
        }
    } catch (err) {

    };
    //Hover menu

    function clickMenu() {
        $(".catalog__link__arow").click(function() {
            $("#heder__catalog__dropdown").stop().slideToggle();
            return false;
        });

        $(".client__link__arow").click(function() {
            $("#heder__client__dropdown").stop().slideToggle();
            return false;
        });
    }

    function hoverMenu() {

        $("#catalog__link").hover(function() {
            $("#heder__catalog__dropdown").stop().slideDown();

        }, function() {
            $("#heder__catalog__dropdown").stop().slideUp();

        });

        $("#client__link").hover(function() {
            $("#heder__client__dropdown").stop().slideDown();

        }, function() {
            $("#heder__client__dropdown").stop().slideUp();

        });

    }

    if ($(window).width() <= 768) {
        clickMenu();
    } else {
        clickMenu();
        hoverMenu();
    }


    //Плавающий ховер
    function initLavaLamp() {
        var w = $(window).width();
        if (w < 575) {
            $('.header__menu__nav').lavalamp().lavalamp('destroy');
        } else {
            $('.header__menu__nav').lavalamp().lavalamp('destroy').lavalamp();
        }

    }

    initLavaLamp();


    $(window).resize(function() {
        initLavaLamp();
    });

    //Мобильное меню
    $('.header__btn-box').click(function() {
        $(".header__menu").slideToggle();
        $(this).toggleClass('active');

    });
    //Появление меню по resize
    $(window).resize(function() {
        var w = $(window).width();
        if (w > 480) {
            $('.header__menu').removeAttr('style');
        }
    });

    //  Карусель

    $(".header__slider").owlCarousel({
        loop: true,
        items: 1,
        autoplay: false,
        nav: true,
        navText: ""
    });

    // Яндекс карта для одной точки

    var $map = $('#map');
    if ($map.length) {
        var myMap;
        ymaps.ready(function() {
            myMap = new ymaps.Map('map', {
                    center: [55.892188, 37.543893],
                    zoom: 12,
                    controls: ['zoomControl'],
                    behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier", "multiTouch"]
                }, {
                    searchControlProvider: 'yandex#search'
                }),
                myMap.geoObjects.add(new ymaps.Placemark([55.892188, 37.543893], {
                    iconContent: ''
                }, {
                    preset: "islands#redStretchyIcon",
                    // // Опции.
                    // // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // // Своё изображение иконки метки.
                    // iconImageHref: 'img/favicon/icon-map.png',
                    // // Размеры метки.
                    // iconImageSize: [25, 36],
                    // // Смещение левого верхнего угла иконки относительно
                    // // её "ножки" (точки привязки).
                    // iconImageOffset: [-12.5, -36],
                }));

            function disableDrag() {
                var w = $(window).width();
                if (w < 768) {
                    myMap.behaviors.disable('drag');
                } else {
                    myMap.behaviors.enable('drag');
                }
            }
            disableDrag();
            $(window).resize(function() {
                disableDrag();
            });
        });
    }


        // Яндекс карта для одной точки

        var $map = $('#contacts__map');
        if ($map.length) {
            var myMap;
            ymaps.ready(function() {
                myMap = new ymaps.Map('contacts__map', {
                        center: [55.892188, 37.543893],
                        zoom: 12,
                        controls: ['zoomControl'],
                        behaviors: ["drag", "dblClickZoom", "rightMouseButtonMagnifier", "multiTouch"]
                    }, {
                        searchControlProvider: 'yandex#search'
                    }),
                    myMap.geoObjects.add(new ymaps.Placemark([55.892188, 37.543893], {
                        iconContent: ''
                    }, {
                        preset: "islands#redStretchyIcon",
                        // // Опции.
                        // // Необходимо указать данный тип макета.
                        iconLayout: 'default#image',
                        // // Своё изображение иконки метки.
                        // iconImageHref: 'img/favicon/icon-map.png',
                        // // Размеры метки.
                        // iconImageSize: [25, 36],
                        // // Смещение левого верхнего угла иконки относительно
                        // // её "ножки" (точки привязки).
                        // iconImageOffset: [-12.5, -36],
                    }));

                function disableDrag() {
                    var w = $(window).width();
                    if (w < 768) {
                        myMap.behaviors.disable('drag');
                    } else {
                        myMap.behaviors.enable('drag');
                    }
                }
                disableDrag();
                $(window).resize(function() {
                    disableDrag();
                });
            });
        }

    //Эффекты левого sidebar

    $(".catalog-p__menu__main__ul_click").click(function() {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open').text("[+]").siblings(".catalog-p__menu__dropdown").stop().slideUp().end().siblings(".fa").removeClass('fa-folder-open').addClass('fa-chevron-down');
        } else {
            $(this).addClass('open').text("[–]").siblings(".catalog-p__menu__dropdown").stop().slideDown().end().siblings('.fa').removeClass('fa-chevron-down').addClass('fa-folder-open');
        }
    });

    //Всплывающая подсказка

    $('[data-toggle="tooltip"]').tooltip(); //data-toggle="tooltip" data-placement="bottom"(для html)

  //Аккардеон в клиентах

    $(".for-client__content__item ").click(function(){
      $(this).find(".for-client__content__item__description").slideToggle();
      $(this).siblings(".for-client__content__item ").find(".for-client__content__item__description").stop().slideUp();

    });

    //  Карусель в сертификатах

    $(".certificates__slider").owlCarousel({
        loop: true,
        items: 3,
        autoplay: false,
        nav: true,
        navText: "",
        responsive:{
       320:{
           items:1
       },
       480:{
           items:2
       },
       768:{
           items:3
       },
       1920:{
           items:3
       }
   }
    });

    // Попапы в сертификатах

    $('.certificates__slider__img').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        }

    });

    $('.popup-youtube').magnificPopup({
        disableOn: 200,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });




};
